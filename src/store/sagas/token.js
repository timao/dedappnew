import {call, put} from 'redux-saga/effects';
import {Alert} from 'react-native';
import api from '../../config/api';
import queryString from 'query-string';
// import {AsyncStorage} from 'react-native';
import {AsyncStorage} from 'react-native';
import {navigatorRef} from '../../index';
import {NavigationActions, StackActions} from 'react-navigation';
import TokenActions from '../ducks/token';

const nav = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({
      routeName: 'MainDrawer',
    }),
  ],
  key: null,
});

export function* getToken(props) {
  const body = queryString.stringify({
    Email: props.action.login,
    Senha: props.action.password,
  });

  try {
    const response = yield call(api.post, `/especificadores/login`, body);
    yield put(TokenActions.addTokenSuccess(response.data));
    console.log(response, 'response')
    const storeAndNavigate = async () => {
      await AsyncStorage.setItem('@codigo', JSON.stringify(response.data.userViewModel.Cod));
      if (props.action.checked) {
        await AsyncStorage.setItem('@token', JSON.stringify(response.data.token));
      } 
      navigatorRef.dispatch(nav);
    };
    setTimeout(storeAndNavigate,1000)

    // navigatorRef.dispatch(nav);
    // Alert.alert('D&D', 'entrou');
  } catch (err) {
    // yield put(TokenActions.addTokenFailure(err));
    Alert.alert('D&D', 'Usuário/Senha inválidos.');
  }
}

export function* getTokenFacebook(props) {
  // console.log(props, 'props login fb')
  // Alert.alert('D&D', props.action.email);
  const body = queryString.stringify({
    email: props.action.email,
  });

  const nav = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: 'MainDrawer',
      }),
    ],
    key: null,
  });
  try {
    const response = yield call(api.post, `/especificadores/loginfacebook`, body);
    yield put(TokenActions.addTokenfacebookSuccess(response.data));
    console.log('response.data', response.data)
    navigatorRef.dispatch(nav);
  } catch (err) {
    yield put(TokenActions.addTokenFailure(err));
    // Alert.alert('D&D', 'Usuário/Senha inválidos.');
  }
}
