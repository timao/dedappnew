import {all, takeLatest, spawn} from 'redux-saga/effects';

import {TokenTypes} from '../ducks/token';
import {getToken, getTokenFacebook} from './token';

import {RegisterTypes} from '../ducks/registerFisic';
import {register} from './registerFisics';

import {MydataTypes} from '../ducks/mydata';
import {postMyData, getData} from './mydata';

import {ContatoTypes} from '../ducks/contato';
import {postContato} from './contato';

import {PontosTypes} from '../ducks/pontos';
import {getPontos} from './pontos';

export default function* rootSaga() {
  yield all([
    takeLatest(TokenTypes.ADD_TOKEN_REQUEST, getToken),
    takeLatest(TokenTypes.ADD_TOKENFACEBOOK_REQUEST, getTokenFacebook),
    takeLatest(RegisterTypes.ADD_REGISTER_REQUEST, register),
    takeLatest(MydataTypes.ADD_MYDATA_REQUEST, postMyData),
    takeLatest(MydataTypes.ADD_GETDATA_REQUEST, getData),
    takeLatest(ContatoTypes.ADD_CONTATO_REQUEST, postContato),
    takeLatest(PontosTypes.ADD_PONTOS_REQUEST, getPontos),
  ]);
}
