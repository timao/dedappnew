import { call, put } from "redux-saga/effects";
import api from "../../config/api";
import queryString from 'query-string';
import {Alert} from "react-native"
import ContatoActions from '../ducks/contato';

export function* postContato(props) {
  const body = queryString.stringify({
    nome: props.action.nome,
    email: props.action.email,
    assunto: props.action.assunto,
    origem: props.action.origem,
    mensagem: props.action.mensagem,
  });

  try {
    const response = yield call(api.post, `/faleconosco`, body);
    yield put(ContatoActions.addContatoSuccess(response.data));
    Alert.alert('D&D', 'Mensagem enviada com sucesso!');
  } catch(err) {
    console.log(err, `errorrr`)
    yield put(ContatoActions.addContatoFailure(err));
    Alert.alert('D&D', 'Ocorreu algum problema. Tente novamente');
  }
}