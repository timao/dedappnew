import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* Types & Action Creators */

const { Types, Creators } = createActions({
  addTokenRequest: ["action"],
  addTokenSuccess: ["token"],
  addTokenFailure: ["token"],
  addTokenfacebookRequest: ["action"],
  addTokenfacebookSuccess: ["token"],
});


export const TokenTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  data: [],
  loading: false,
});

/* Reducers */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_TOKEN_REQUEST]: state => state.merge({ loading: true }),
  [Types.ADD_TOKEN_SUCCESS]: (state: Object, { token }: any) => state.merge({ loading: false,  data: token }),
  [Types.ADD_TOKEN_FAILURE]: (state: Object, { token }: any) => state.merge({ loading: false,  data: token }),
  [Types.ADD_TOKENFACEBOOK_REQUEST]: state => state.merge({ loading: true }),
  [Types.ADD_TOKENFACEBOOK_SUCCESS]: (state: Object, { token }: any) => state.merge({ loading: false,  data: token }),
});