import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* Types & Action Creators */

const { Types, Creators } = createActions({
  addRegisterRequest: ["action"],
  addRegisterSuccess: ["register"],
  addRegisterFailure: ["register"]
});


export const RegisterTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  data: [],
  loading: false
});

/* Reducers */

export const reducer = createReducer(INITIAL_STATE, {

  [Types.ADD_REGISTER_REQUEST]: state => state.merge({ loading: true }),
  [Types.ADD_REGISTER_SUCCESS]: (state: Object, { register }: any) => state.merge({ loading: false,  data: register }),
  [Types.ADD_REGISTER_FAILURE]: (state: Object, { register }: any) => state.merge({ loading: false,  data: register }),

});