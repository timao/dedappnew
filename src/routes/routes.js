import React from 'react';
import {Image} from 'react-native';
import {
  createAppContainer,
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import {colors, fonts} from '../styles';
import Splash from '../pages/Splash';
import LoginScene from '../pages/Login';
import RegisterScene from '../pages/Register/index';
import RegisterJuricScene from '../pages/Register/registerJuric';
import RegisterFisicScene from '../pages/Register/registerFisic';
import ContactUsScene from '../pages/ContactUs';
import RulesScene from '../pages/Rules';

import MyDataScene from '../pages/MyData';
import DrawerSidebar from '../component/DrawerSidebar';
import {HomeStackNavigator} from './stacks/HomeStack';
import {PointsStackNavigator} from './stacks/PointsStack';
import {BenefitsStackNavigator} from './stacks/BenefitsStack';
import {StoreStackNavigator} from './stacks/StoreStack';
import {NewsStackNavigator} from './stacks/NewsStack';

const TabBarMain = createBottomTabNavigator(
  {
    HomeStackNavigator: {
      screen: HomeStackNavigator,
      navigationOptions: {
        tabBarLabel: 'INICIO',
        tabBarIcon: ({focused, tintColor}) =>
          focused ? (
            <Image
              source={require('../assets/home-active.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ) : (
            <Image
              source={require('../assets/home.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ),
      },
    },
    PointsStackNavigator: {
      screen: PointsStackNavigator,
      navigationOptions: {
        tabBarLabel: 'PONTOS',
        tabBarIcon: ({focused, tintColor}) =>
          focused ? (
            <Image
              source={require('../assets/pontos-active.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ) : (
            <Image
              source={require('../assets/pontos.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ),
      },
    },

    // BenefitsStackNavigator: {
    //   screen: BenefitsStackNavigator,
    //     navigationOptions: {
    //       tabBarLabel: 'BENEFÍCIOS',
    //       tabBarIcon: ({ focused, tintColor }) => (
    //         focused ? (
    //           <Image source={require('../assets/beneficios-active.png')} style={{ width: 35, height: 35, resizeMode: 'contain' }} />
    //         ) : (
    //           <Image source={require('../assets/beneficios.png')} style={{ width: 35, height: 50, resizeMode: 'contain' }} />
    //         )
    //       ),
    //     },
    // },

    RulesScene: {
      screen: RulesScene,
      navigationOptions: {
        tabBarLabel: 'REGULAMENTO',
        tabBarIcon: ({focused, tintColor}) =>
          focused ? (
            <Image
              source={require('../assets/regulamento-active.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ) : (
            <Image
              source={require('../assets/regulamento.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ),
      },
    },

    // StoreStackNavigator: {
    //   screen: StoreStackNavigator,
    //   navigationOptions: {
    //     tabBarLabel: 'LOJAS',
    //     tabBarIcon: ({focused, tintColor}) =>
    //       focused ? (
    //         <Image
    //           source={require('../assets/lojas-active.png')}
    //           style={{width: 35, height: 35, resizeMode: 'contain'}}
    //         />
    //       ) : (
    //         <Image
    //           source={require('../assets/lojas.png')}
    //           style={{width: 35, height: 35, resizeMode: 'contain'}}
    //         />
    //       ),
    //   },
    // },

    MyDataScene: {
      screen: MyDataScene,
      navigationOptions: {
        tabBarLabel: 'MEUS DADOS',
        tabBarIcon: ({focused, tintColor}) =>
          focused ? (
            <Image
              source={require('../assets/beneficios-active.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ) : (
            <Image
              source={require('../assets/beneficios.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ),
      },
    },


    // NewsStackNavigator: {
    //   screen: NewsStackNavigator,
    //   navigationOptions: {
    //     tabBarLabel: 'NOVIDADES',
    //     tabBarIcon: ({focused, tintColor}) =>
    //       focused ? (
    //         <Image
    //           source={require('../assets/novidades-active.png')}
    //           style={{width: 35, height: 35, resizeMode: 'contain'}}
    //         />
    //       ) : (
    //         <Image
    //           source={require('../assets/novidades.png')}
    //           style={{width: 35, height: 35, resizeMode: 'contain'}}
    //         />
    //       ),
    //   },
    // },

    ContactUsScene: {
      screen: ContactUsScene,
      navigationOptions: {
        tabBarLabel: 'CONTATO',
        tabBarIcon: ({focused, tintColor}) =>
          focused ? (
            <Image
              source={require('../assets/novidades-active.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ) : (
            <Image
              source={require('../assets/novidades.png')}
              style={{width: 35, height: 35, resizeMode: 'contain'}}
            />
          ),
      },
    },

   
  },
  {
    initialRouteName: 'HomeStackNavigator',
    animationEnabled: false,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: colors.white,
      inactiveTintColor: colors.white,
      labelStyle: {
        fontSize: 14,
        paddingTop: 5,
        fontFamily: fonts.tradeconde18,
      },
      style: {
        backgroundColor: colors.black,
        borderTopColor: '#000',
        paddingTop: 12,
        height: 80,
        paddingHorizontal: 15,
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: {height: 0, width: 0},
      },
    },
  },
);

const MainDrawer = createDrawerNavigator(
  {
    TabBarMain: {screen: TabBarMain},
  },
  {
    drawerPosition: 'left',
    contentComponent: props => <DrawerSidebar {...props} />,
  },
);

const MainStack = createStackNavigator(
  {
    Splash: Splash,
    MainDrawer: MainDrawer,
    LoginScene: LoginScene,
    RegisterScene: RegisterScene,
    RegisterJuricScene: RegisterJuricScene,
    RegisterFisicScene: RegisterFisicScene,
    TabBarMain: TabBarMain,
    MyDataScene: MyDataScene,
    ContactUsScene: ContactUsScene,
    HomeStackNavigator: HomeStackNavigator,
  },
  {
    headerMode: 'none',
  },
);

const Routes = createAppContainer(createSwitchNavigator({MainStack}));

export default Routes;
