import styled from 'styled-components';
import colors from '../../styles/colors';
import { fonts } from '../../styles';

export const H1 = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 31px;
  color: ${colors.secondary};
  margin: 0 0 -13px;
  padding: 0 0;
`;

export const H2 = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 55px;
  color: ${colors.white};
  margin: 0 0;
  padding: 0 0;
`;

export const Span = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 16px;
  color: ${colors.white};
  margin: 0 0 35px;
  padding: 0 0;
  line-height: 21px;
`;

export const Span2 = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 18px;
  color: ${colors.white};
  margin: 30px 0 35px;
  padding: 0 0;
  line-height: 21px;
`;

export const H3 = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 25px;
  color: ${colors.secondary};
  margin: 0 0 -16px;
  padding: 0 0;
`;
