import styled from 'styled-components';
import colors from '../../styles/colors';
import fonts from '../../styles/fonts';

export const Logo = styled.Image`
  text-align: center;
  margin-bottom: 15px;
  width: 100%;
  height: 80px;
`;

export const H1 = styled.Text`
  font-size: 16px;
  color: ${colors.white};
  font-family: ${fonts.tradecb};
  margin-top: 25px;
  margin-bottom: 25px;
  text-align: center;
`;

export const Container = styled.View`
  padding: 40px 20px 0;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  background: ${colors.black};
  border-radius: 10px;
  width: 90%;
  margin: 0 auto;
`;

export const Title = styled.Text`
  font-size: 16px;
  color: ${colors.darker};
  font-weight: bold;
  margin-bottom: 15px;
`;

export const Facebook = styled.TouchableOpacity`
  width: 100%;
  border-radius: 10px;
  height: 65px;
  background: transparent;
  border: 1px solid #fff;
  color: ${colors.white};
  padding: 15px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const TextFacebook = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  /* font-family: 'Trade-Gothic-Condensed-Bold-20'; */
  font-family: ${fonts.tradecb};
  text-align: center;
  margin-left: 15px;
`;

export const Footer = styled.View`
  justify-content: center;
  flex-direction: column;
  align-items: center;
  width: 90%;
  margin: 30px auto;
`;

export const TextFooter = styled.Text`
  font-size: 9px;
  color: ${colors.white50};
  /* font-family: 'Trade-Gothic'; */
  font-family: ${fonts.tradegothic};
  margin-bottom: 15px;
`;

export const Button = styled.TouchableOpacity`
  height: 65px;
  background: ${colors.secondary};
  width: 100%;
  border-radius: 10px;
  padding: 15px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 35px;
  margin-bottom: 0px;
`;

export const TextButton = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  text-align: center;
  /* font-family: 'Trade-Gothic-Condensed-Bold-20'; */
  font-family: ${fonts.tradecb};
  margin-left: 0;
`;

export const IndicatorLoading = styled.ActivityIndicator`
  position: absolute;
  right: 25;
`;

export const WrapCheckBox = styled.TouchableOpacity`
  flex-direction: row;
  display: flex;
  width: 70%;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  margin-bottom: 30px;
`;

export const ButtonCheckBox = styled.View`
  border: 1px solid ${colors.white};
  height: 15px;
  width: 15px;
  border-radius: 5px;
  position: relative;
`;

export const ContainerCheckBox = styled.View`
  width: 15px;
  height: 15px;
  position: absolute;
  border-radius: 5px;
  background: ${colors.white};
`;

export const Span = styled.Text`
  font-size: 15px;
  color: ${colors.white};
  margin-left: 15px;
`;
