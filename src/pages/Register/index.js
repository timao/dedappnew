import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import { ContentBg } from '../../styles/general';
import {
  Logo,
  H1,
  Span,
  Button,
  TextButton,
  Container,
  ContainerCenter
} from "./styles";

const RegisterScene = ({navigation}) => {
  return (
    <ContentBg>
        <ContainerCenter>
          <Logo
            source={require('../../assets/logo.png')}
            resizeMode="contain"
          />
          <H1>CADASTRO</H1>
          <Span>ESCOLHA UMA DAS OPÇÕES ABAIXO.</Span>

          <Button onPress={() => navigation.push('RegisterFisicScene')}>
            <TextButton>PESSOA FÍSICA</TextButton>
          </Button>

          <Button onPress={() => navigation.push('RegisterJuricScene')} style={{marginTop: 20}}>
            <TextButton>PESSOA JURÍDICA</TextButton>
          </Button>

        </ContainerCenter>
    </ContentBg>
  );
};

export default RegisterScene;
