import React from 'react';
import {View, StyleSheet} from 'react-native';
import {colors, fonts} from '../styles';

import RNPickerSelect from 'react-native-picker-select';

const RNPicker = ({list, valueChange, value, styleRow, ...props}) => {
    console.log(list , 'lista')
    console.log(props, 'props')
    return (

  <View style={[styles.containerSelect, styleRow]}>
    {/* <RNPickerSelect
      {...props}
      selectedValue={value}
      onValueChange={value => {
        valueChange(value);
      }}
      itemStyle={{fontFamily: fonts.tradeconde18}}
      style={styles.select}>
      <Picker.Item label={label} value={''} />
      {list.map((item, index) => (
        <Picker.Item label={item.nome} value={index} />
      ))}
    </RNPickerSelect> */}

    <RNPickerSelect
      placeholder={'teste'}
      onValueChange={value => {
        valueChange(value);
      }}
      items={[
        { label: 'Football', value: 'football' },
        { label: 'Baseball', value: 'baseball' },
        { label: 'Hockey', value: 'hockey' },
      ]}>
      </RNPickerSelect>
  </View>
    )
};

const styles = StyleSheet.create({
  containerSelect: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.white,
    height: 60,
    marginBottom: 15,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  select: {
    height: 35,
    fontFamily: fonts.tradeconde18,
    fontSize: 12,
    color: colors.white50,
    width: '100%',
  },
});

export default RNPicker;
