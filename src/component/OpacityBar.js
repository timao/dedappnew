import React, {Component} from 'react';
import {View, Text} from 'react-native';

export const OpacityBar = ({opacity}) => {
  return (
    <View
      style={{
        position: 'absolute',
        bottom: 0,
        width: '100%',
        border: 1,
        backgroundColor: 'black',
        height: 50,
        opacity : 0.1,
        height : 0
      }}>
      <Text style={{color: '#FFF'}}>  {opacity}</Text>
    </View>
  );
};
