import React from 'react';
import { Picker, PickerIOS, View, StyleSheet } from 'react-native';
import { colors, fonts } from '../styles';

const PickerSelect = ({
  list,
  valueChange,
  label,
  value,
  styleRow,
  ...props
}) => (
  <View style={[styles.containerSelect, styleRow]}>
    <Picker
      {...props}
      selectedValue={value}
      onValueChange={(value) => {
          valueChange(value)
      }}
      itemStyle={{ fontFamily: fonts.tradeconde18 }}
      style={styles.select}>
        <Picker.Item label={label} value={''} />
      {
        list.map(
          (item, index) => (
            <Picker.Item label={item.nome} value={index} />
          )
        )
      }
    </Picker>
  </View>
);

const styles = StyleSheet.create({
  containerSelect: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.white,
    height: 60,
    marginBottom: 15,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  select: {
    height: 35,
    fontFamily: fonts.tradeconde18,
    fontSize: 12,
    color: colors.white50,
    width: '100%',
  }
})

export default PickerSelect;