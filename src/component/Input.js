import React from 'react';
import { TouchableOpacity } from 'react-native';

import styled from 'styled-components';
import colors from '../styles/colors';
import fonts from '../styles/fonts';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export const Input = ({...props}) => {
  return (
    <Row>
      {
        props.closeButton && (<TouchableOpacity style={{position: 'absolute', right: 20,}} onPress={props.clear}><Icon name='close' size={20} color={colors.white} /></TouchableOpacity>)
      }
      <InputText 
        {...props}
        style={{
          height: 60,
          borderWidth: 1,
          borderColor: colors.white,
          width: '100%',
          backgroundColor: 'transparent',
          fontSize: 16,
          fontFamily: fonts.tradeconde18,
          color: colors.white50,
          borderRadius: 10,
          paddingTop: 18,
          paddingHorizontal: 25,
          paddingBottom: 15,
          marginBottom: 10,
        }}
      />
    </Row>
  );
};

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;
`;

export const InputText = styled.TextInput`
  /* height: 60px;
  border: 1px solid #fff;
  width: 100%;
  background: transparent;
  font-size: 16px;
  font-family: ${fonts.tradeconde18};
  z-index: 5;
  padding: 18px 25px 15px;
  color: ${colors.white50};
  border-radius: 10px;
  padding: 18px 25px 15px; */
`;
