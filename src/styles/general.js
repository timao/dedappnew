import styled from 'styled-components';
import colors from './colors';
import fonts from './fonts';

export const ContentBg = styled.SafeAreaView`
  flex: 1;
  background: ${colors.black};
  justify-content: center;
  align-items: center;
  flex-direction: column;
 `;

export const Content = styled.SafeAreaView`
  flex: 1;
  background: #000;
`;

export const ContentScroll = styled.ScrollView`
  flex: 1;
  justify-content: center;
`;

export const Container = styled.View`
  padding: 20px;
`;


export const ListView = styled.FlatList`
  margin-top: 30px;
`;

export const WrapView = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  margin-bottom: 0;
  padding: 0 0 35px 15px;
  border-left-width: 1px;
  border-color: ${colors.secondary};
`;

export const ContentList = styled.View`
  flex-direction: column;
`;

export const Title = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 25px;
  color: ${colors.white};
  margin: 0 0;
  padding: 0 0;
`;

export const Value = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 21px;
  color: ${colors.white};
  margin: 0 0 10px;
`;

export const Border = styled.View`
  width: 13px;
  height: 13px;
  border-radius: 100px;
  border-width: 1px;
  border-color: ${colors.secondary};
  position: absolute;
  left: -6;
  z-index: 99;
  background: #000;
`;

export const Scroll = styled.ScrollView`
  flex: 1;
  background: #000;
`;

export const FraseRodape = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 12px;
  color: ${colors.white50};
  margin: 0 0 10px;
`;

export const ViewRodape = styled.View`
 justify-content: center;
 position: absolute;
 bottom: 20px;
`;